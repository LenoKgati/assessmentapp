import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import  Navigation  from "./components/Navigation";

import  Home  from "./components/Home";

import ArticleDetails from "./components/ArticleDetails";

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route exact path="/"  component={() => <Home />} />
         
          <Route exact path="/articles/:id" component={ArticleDetails}/>
          
          
        </Switch>
       
      </Router>
    </div>
  );
}

export default App;