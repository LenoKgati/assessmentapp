import React from "react";
import {  withRouter } from "react-router-dom";
import { AppBar, Toolbar,  Typography } from '@material-ui/core';
import useStyles from '../components/navStyles';
import logo from '../assets/logo.png';


function Navigation(props) {
  const classes = useStyles;
  return (
    <AppBar position="fixed" className={classes.AppBar} color="inherit">
    <Toolbar>
      <Typography variant="body2" className={classes.title} color="inherit">
      <img  src={logo} alt="newsapp"  height="10px" className={classes.image} />NewsApp
      </Typography>
      
    </Toolbar>
  </AppBar>
  );
}

export default withRouter(Navigation);