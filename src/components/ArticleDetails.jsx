/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-no-target-blank */
import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom"
import { useHistory } from 'react-router-dom';




function ArticleDetails  (props)  {
  const [articles,  setArticles] = useState ([])
const [popular, setPopular] = useState(
    'Popular')

 const [isLoading, setIsLoading] = useState(true)   

 useEffect(() => {
     
  fetch('https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?api-key=dOWHYNJHmSisO6tQw0kC9v8601E1HCxL').then(res=>{
    const data = res.json();
    articles.splice(0, articles.length);
    data.then(info=>{
      const filteredData = info.results.filter(article => String(article.id) === String(props.match.params.id));
      articles.push(filteredData)
    setArticles([...filteredData]);
    });    
  });
  
      
 }, )
 const history = useHistory();
 const handleClick = () => history.push('/' );
  return (
    <>
    <div className="showcase">
   
    
    
    </div>
    <section className="grid grid-cols-1 gap-10 px-5 pt-10 pb-20">
     {articles.map((article) =>{
    const {abstract, title, byline, section , published_date, id, url, updated} =article;

    return (
      <article key={id} className="bg-white py-10 px-5 rounded-lg lg:w-9/12 lg:mx-auto">
        
     <h2 className="font-bold text-2*1 mb-5 lg:text-2*1"> {title}</h2>
     <p> {abstract}</p>
    

     <ul className="my-4">
       <li><span className="font-bold">Author/s: </span> {byline}</li>
       <li> <span className="font-bold"> Section: </span>  {section}</li>
       <li> <span className="font-bold">Date Published: </span> {published_date}</li>
       <li> <span className="font-bold">Updated: </span> {updated}</li>
     </ul>

     <button style={{color:"#00adb5", }}  type="button" onClick={handleClick} >
     Go Back
    </button>

    
     

      </article>
    )
     })}
    </section>

    </>
  );
}

export default ArticleDetails;
