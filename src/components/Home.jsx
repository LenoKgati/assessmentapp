import React, {useState, useEffect} from 'react';
import {  FaCalendar,FaBookReader} from 'react-icons/fa';
import { Card, CardMedia, CardContent, CardActions, Typography,  } from '@material-ui/core';

import { useHistory } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import useStyles from '../components/styles';


function Home() {

  const classes = useStyles();
  
  const [articles,  setArticles] = useState (
    [])
const [popular, setPopular] = useState(
    'Popular')

 const [isLoading, setIsLoading] = useState(true)   

 useEffect(() => {
     
    const getPopularArticles = async () => {
  
    
         const res = await fetch('https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?api-key=dOWHYNJHmSisO6tQw0kC9v8601E1HCxL')
         
         const articles = await res.json();

      
         setArticles(articles.results);
    
}
        getPopularArticles()
 }, )

 const history = useHistory();
 

  return (
    <>
     <main className={classes.content}>
      <div className={classes.toolbar} />
    <Grid container justify="center" spacing={4}>
  
   

     {articles.map((article) =>{
    const { title, url, section , published_date, id, media} =article;
   
  
    const handleClick = () => history.push('/articles/' + id);
   
    console.log(article);
    return (

      
      <Grid item xs={12} sm={6} md={4} lg={3} >

<Card className={classes.root} >
      <ardMedia className={classes.media} image={url} />
      <CardContent>
        <div className={classes.cardContent}>
      
          <Typography gutterBottom variant="h5" component="h3" style={{color:"#00adb5", }} >
            {section}
          </Typography>
         
          <Typography gutterBottom variant="body2" component="p">
            {title}
          </Typography>
        </div>
        <FaCalendar  color="#00adb5" />
        <Typography  dangerouslySetInnerHTML={{ __html: published_date }} variant="body3" color="textSecondary" component="p" />
      </CardContent>
      <CardActions disableSpacing className={classes.cardActions}>
        <FaBookReader onClick={handleClick} color="#00adb5" aria-label="Read More">
         
        </FaBookReader>
      </CardActions>
      <button type="button"  style={{color:"#00adb5", border:"2px" }}  onClick={handleClick} >
      Open Article
    </button>
      
    </Card>
         
      </Grid>
    )
     })}
    </Grid>
</main>
    </>
  );
}

export default Home;
