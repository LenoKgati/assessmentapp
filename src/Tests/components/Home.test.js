import React from 'react';
import { shallow } from 'enzyme';
import {render} from '@testing-library/react';
import Home from "../../components/Home"

test('should test Home component with default state of empty array', () => {
    const wrapper = shallow(<Home todos={[]} />);
    expect(wrapper).toMatchSnapshot();
   });

   test('should test Home component with list of news articles', () => {
    const wrapper = shallow(
     <Home todos={['The real chances of a breakthrough infection.',
      'A Montana town reflects on its effort to drive former President Donald J. Trump’s extremist supporters back to the fringes.']} />
    );
    expect(wrapper).toMatchSnapshot();
   });  

   describe("Home", () => {
    it("should render my component", () => {
      const wrapper = shallow(<Home />);
    });
  });
  