import React from 'react';
import { shallow } from 'enzyme';
import ArticleDetails from "../../components/ArticleDetails"

test('should test ArticleDetails component with default state of empty array', () => {
    const wrapper = shallow(<ArticleDetails todos={[]} />);
    expect(wrapper).toMatchSnapshot();
   });

   test('should test ArticleDetail component with list of news articles', () => {
    const wrapper = shallow(
     <ArticleDetails todos={['The real chances of a breakthrough infection.']} />
    );
    expect(wrapper).toMatchSnapshot();
   });  

   describe("ArticleDetails", () => {
    it("should render my component", () => {
      const wrapper = shallow(<ArticleDetails />);
    });
  });

  