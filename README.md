### Running App
1. git clone https://gitlab.com/LenoKgati/assessmentapp.git
2. cd assessmentapp
3. npm install
4. npm start


### Running Tests
1. npm run test


### Generating Coverage Report
1. npm run test -- --coverage --watchAll=false
